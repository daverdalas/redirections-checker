<?php

declare(strict_types=1);

return [
    'concurrency' => 10,
    'request_timeout' => 30,
    'urls_per_job' => 10,
    'urls_per_minute' => 1000,
];

<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Redirection;
use Illuminate\Database\Seeder;

class RedirectionsTableSeeder extends Seeder
{
    public function run(): void
    {
        Redirection::factory()
            ->count(200)
            ->create();
    }
}

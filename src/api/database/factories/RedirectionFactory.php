<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Redirection;
use Illuminate\Database\Eloquent\Factories\Factory;

class RedirectionFactory extends Factory
{
    /** @var string */
    protected $model = Redirection::class;

    public function definition(): array
    {
        return [
            'url' => 'http://test-redirects.137.software?rand=' . $this->faker->unique()->numberBetween(0, 10000),
            'redirections' => $this->faker->numberBetween(0, 10),
            'time' => $this->faker->numberBetween(0, 9999) / 100,
            'checked_at' => $this->faker->boolean ? $this->faker->dateTime : null,
        ];
    }
}

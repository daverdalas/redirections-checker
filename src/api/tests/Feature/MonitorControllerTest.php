<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\Redirection;
use App\Services\RedirectsChecker\RedirectsChecker;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MonitorControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStore(): void
    {
        $this->app->bind(RedirectsChecker::class, function (): RedirectsChecker {
            $mock = new MockHandler([
                new Response(301, ['Location' => 'http://example.com']),
                new Response(301, ['Location' => 'http://example.com']),
                new Response(302, ['Location' => 'http://example.com']),
                new Response(200),
            ]);
            $handlerStack = HandlerStack::create($mock);
            $guzzleClient = new GuzzleClient(['handler' => $handlerStack]);

            return new RedirectsChecker($guzzleClient, 30, 1);
        });

        $response = $this->postJson(
            route('monitors.store', ['stats' => true]),
            [
                'http://example.com',
            ]
        );

        $response->assertStatus(200);
        $response->assertHeader('X-Stats', json_encode(['http://example.com' => 3]));
        $this->assertDatabaseHas(
            (new Redirection())->getTable(),
            [
                'url' => 'http://example.com',
                'redirections' => 3,
            ]
        );
    }

    public function testShow(): void
    {
        Redirection::query()
            ->insert([
                [
                    'url' => 'http://example.com',
                    'redirections' => 3,
                    'time' => 1.2343,
                    'checked_at' => new \DateTimeImmutable('2021-01-11 18:37:07'),
                ],
                [
                    'url' => 'http://example.com',
                    'redirections' => 2,
                    'time' => 0.134,
                    'checked_at' => new \DateTimeImmutable('2021-01-11 19:37:07'),
                ],
                [
                    'url' => 'http://example.com',
                    'redirections' => 5,
                    'time' => 0.4,
                    'checked_at' => new \DateTimeImmutable('2021-01-11 16:37:07'),
                ],
            ]);

        $response = $this->get(route('monitors.show', 'http://example.com'));

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                [
                    'numberOfRedirections' => 2,
                    'time' => '0.13400',
                    'checkedAt' => '2021-01-11 19:37:07',
                ],
                [
                    'numberOfRedirections' => 3,
                    'time' => '1.23430',
                    'checkedAt' => '2021-01-11 18:37:07',
                ],
                [
                    'numberOfRedirections' => 5,
                    'time' => '0.40000',
                    'checkedAt' => '2021-01-11 16:37:07',
                ],
            ],
        ]);
    }
}

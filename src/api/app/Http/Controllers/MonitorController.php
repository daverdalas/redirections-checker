<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Presenters\RedirectionsPresenter;
use App\Repositories\RedirectionRepositoryInterface;
use App\Services\RedirectsChecker\RedirectsChecker;
use App\Services\RedirectsChecker\UrlStats;
use App\Entities\Redirection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class MonitorController extends Controller
{
    private const INITIAL_STATS_TIMEOUT = 2;
    private const REDIRECTIONS_TO_SHOW = 10;

    public function __construct(private RedirectionRepositoryInterface $redirectionRepository)
    {
    }

    public function store(Request $request, RedirectsChecker $redirectsChecker): JsonResponse
    {
        $response = new JsonResponse();
        $urls = $request->json()->all();

        if ((bool) $request->get('stats')) {
            $results = $redirectsChecker($urls, self::INITIAL_STATS_TIMEOUT);
            $redirections = array_map(
                fn(UrlStats $urlStats): Redirection => Redirection::fromUrlStats($urlStats),
                $results
            );
            $response->header('X-Stats', (new RedirectionsPresenter($redirections))->toHeader());
        } else {
            $redirections = array_map(fn(string $url): Redirection => new Redirection($url), $urls);
        }

        $this->redirectionRepository->saveMany($redirections);

        return $response;
    }

    public function show(string $url): JsonResponse
    {
        $redirections = $this->redirectionRepository->findForUrl($url, self::REDIRECTIONS_TO_SHOW);
        $presented = (new RedirectionsPresenter($redirections))->toArray();

        return (new JsonResponse())->setData(['data' => $presented]);
    }
}

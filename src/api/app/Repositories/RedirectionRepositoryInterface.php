<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Entities\Redirection;

interface RedirectionRepositoryInterface
{
    /**
     * @return Redirection[]
     */
    public function oldestChecked(int $take): array;

    /**
     * @return Redirection[]
     */
    public function findForUrl(string $url, int $take): array;

    /**
     * @param Redirection[] $redirections
     */
    public function saveMany(array $redirections): void;
}

<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Models\Redirection as RedirectionEloquent;
use App\Repositories\RedirectionRepositoryInterface;
use App\Entities\Redirection;

class RedirectionRepository implements RedirectionRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function oldestChecked(int $take): array
    {
        return RedirectionEloquent::query()
            ->selectRaw('*, MAX(checked_at) as last_check_at')
            ->orderBy('last_check_at', 'ASC')
            ->orderBy('id', 'ASC')
            ->limit($take)
            ->groupBy('url')
            ->get()
            ->map(
                fn(RedirectionEloquent $redirection): Redirection => $this->modelToRedirection($redirection)
            )
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function findForUrl(string $url, int $take): array
    {
        return RedirectionEloquent::query()
            ->where('url', $url)
            ->whereNotNull('checked_at')
            ->orderBy('checked_at', 'DESC')
            ->limit($take)
            ->get()
            ->map(
                fn(RedirectionEloquent $redirection): Redirection => $this->modelToRedirection($redirection)
            )
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function saveMany(array $redirections): void
    {
        RedirectionEloquent::query()
            ->insert(array_map(
                fn(Redirection $redirection): array => [
                    'url' => $redirection->url,
                    'time' => $redirection->time,
                    'redirections' => $redirection->numberOfRedirections,
                    'checked_at' => $redirection->checkedAt,
                    'created_at' => new \DateTimeImmutable(),
                    'updated_at' => new \DateTimeImmutable(),
                ],
                $redirections
            ));
    }

    private function modelToRedirection(RedirectionEloquent $redirectionModel): Redirection
    {
        return new Redirection(
            $redirectionModel->getAttribute('url'),
            (float) $redirectionModel->getAttribute('time'),
            $redirectionModel->getAttribute('redirections'),
            $redirectionModel->getAttribute('checked_at') ?
                new \DateTimeImmutable($redirectionModel->getAttribute('checked_at')) : null,
        );
    }
}

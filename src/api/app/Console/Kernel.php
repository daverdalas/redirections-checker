<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\ScheduleUrlsToCheckCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Contracts\Config\Repository as Config;

class Kernel extends ConsoleKernel
{
    protected function schedule(Schedule $schedule): void
    {
        /** @var Config $config */
        $config = $this->app->get(Config::class);
        $schedule->command(
            ScheduleUrlsToCheckCommand::class,
            [$config->get('redirects_checker.urls_per_minute')]
        )->everyMinute();
    }

    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

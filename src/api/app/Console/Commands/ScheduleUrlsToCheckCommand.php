<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Jobs\CheckUrlsJob;
use App\Repositories\RedirectionRepositoryInterface;
use App\Entities\Redirection;
use Illuminate\Console\Command;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Contracts\Queue\Queue;

final class ScheduleUrlsToCheckCommand extends Command
{
    /** @var string */
    protected $signature = 'schedule-urls {number-of-urls}';

    /** @var string */
    protected $description = 'Push n urls into the queue';

    public function handle(
        RedirectionRepositoryInterface $redirectionRepository,
        Config $config,
        Queue $queue
    ): void {
        $take = (int) $this->argument('number-of-urls');
        $urls = $redirectionRepository->oldestChecked($take);
        $chunkSize = $config->get('redirects_checker.urls_per_job');
        $jobs = array_map(
            fn(array $redirections): CheckUrlsJob => new CheckUrlsJob(
                array_map(fn(Redirection $redirection): string => $redirection->url, $redirections)
            ),
            array_chunk($urls, $chunkSize)
        );
        $queue->bulk($jobs);
    }
}

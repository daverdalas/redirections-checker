<?php

declare(strict_types=1);

namespace App\Entities;

use App\Services\RedirectsChecker\UrlStats;

class Redirection
{
    public function __construct(
        public string $url,
        public ?float $time = null,
        public ?int $numberOfRedirections = null,
        public ?\DateTimeImmutable $checkedAt = null
    ) {
    }

    public static function fromUrlStats(UrlStats $urlStats, \DateTimeImmutable $checkedAt = null): static
    {
        return new static(
            $urlStats->url,
            $urlStats->time,
            $urlStats->numberOfRedirections,
            $checkedAt ?: $urlStats->checkedAt,
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Services\RedirectsChecker;

class UrlStats
{
    public function __construct(
        public string $url,
        public ?float $time = null,
        public ?int $numberOfRedirections = null,
        public ?\DateTimeImmutable $checkedAt = null
    ) {
    }

    public function addRedirect(float $time): static
    {
        return new static(
            $this->url,
            $this->time !== null ? $this->time + $time : $time,
            $this->numberOfRedirections !== null ? $this->numberOfRedirections + 1 : 1,
            $this->checkedAt
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Services\RedirectsChecker;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Pool;
use GuzzleHttp\Promise\CancellationException;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\TransferStats;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

final class RedirectsChecker
{
    private const REDIRECT_CODES = [
        SymfonyResponse::HTTP_MOVED_PERMANENTLY,
        SymfonyResponse::HTTP_FOUND,
        SymfonyResponse::HTTP_SEE_OTHER,
        SymfonyResponse::HTTP_NOT_MODIFIED,
        SymfonyResponse::HTTP_USE_PROXY,
        SymfonyResponse::HTTP_RESERVED,
        SymfonyResponse::HTTP_TEMPORARY_REDIRECT,
        SymfonyResponse::HTTP_PERMANENTLY_REDIRECT,
    ];

    public function __construct(
        private GuzzleClient $guzzleClient,
        private int $requestTimeout,
        private int $concurrency
    ) {
    }

    /**
     * @param  string[]  $urls
     * @return UrlStats[]
     */
    public function __invoke(array $urls, int $poolTimeout = null): array
    {
        $results = [];
        $requestTimeout = $poolTimeout < $this->requestTimeout ? $poolTimeout : $this->requestTimeout;
        $requests = function () use ($urls, &$results, $requestTimeout): \Generator {
            foreach ($urls as $key => $url) {
                yield $key => function () use ($key, $requestTimeout, $url, &$results): PromiseInterface {
                    return $this->guzzleClient->getAsync($url, [
                        'on_stats' => function (TransferStats $stats) use (&$results, $key, $url): void {
                            $response = $stats->getResponse();
                            $urlStats = $results[$key] ?? $results[$key] = new UrlStats(
                                    $url,
                                    $response === null ? null : 0,
                                    $response === null ? null : 0,
                                    new \DateTimeImmutable()
                                );
                            if (in_array($stats->getResponse()?->getStatusCode(), self::REDIRECT_CODES)) {
                                $results[$key] = $urlStats->addRedirect($stats->getTransferTime());
                            }
                        },
                        'timeout' => $requestTimeout,
                        'allow_redirects' => [
                            'max' => INF,
                        ],
                        'stream' => true,
                    ]);
                };
            }
        };

        $this->pool($requests, $poolTimeout);

        return $results + array_map(fn(string $url): UrlStats => new UrlStats($url), array_diff_key($urls, $results));
    }

    private function pool(callable $requests, int $poolTimeout = null): void
    {
        $timeStart = microtime(true);

        $pool = new Pool(
            $this->guzzleClient,
            $requests(),
            [
                'concurrency' => $this->concurrency,
                'fulfilled' => function (Response $response, int|string $index, Promise $promise) use (
                    $poolTimeout,
                    $timeStart
                ): void {
                    if (
                        $poolTimeout !== null &&
                        (microtime(true) - $timeStart) >= $poolTimeout
                    ) {
                        $promise->cancel();
                    }
                },
                'rejected' => function (\Exception $exception, int|string $index, Promise $promise) use (
                    $timeStart,
                    $poolTimeout
                ): void {
                    if (
                        $poolTimeout !== null &&
                        (microtime(true) - $timeStart) >= $poolTimeout
                    ) {
                        $promise->cancel();
                    }

                    report($exception);
                },
            ]);

        $promise = $pool->promise();

        try {
            $promise->wait();
        } catch (CancellationException) {
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Services\RedirectsChecker;

use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

final class RedirectsCheckerServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->bind(RedirectsChecker::class, function (Application $app): RedirectsChecker {
            $guzzleClient = new GuzzleClient();
            /** @var Config $config */
            $config = $app->make(Config::class);

            return new RedirectsChecker(
                $guzzleClient,
                $config->get('redirects_checker.request_timeout'),
                $config->get('redirects_checker.concurrency'),
            );
        });
    }

    public function provides(): array
    {
        return [RedirectsChecker::class];
    }
}

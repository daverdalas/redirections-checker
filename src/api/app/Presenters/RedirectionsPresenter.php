<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Entities\Redirection;

final class RedirectionsPresenter
{
    /**
     * @param Redirection[] $redirections
     */
    public function __construct(private array $redirections)
    {
    }

    public function toHeader(): string
    {
        return json_encode(
            array_combine(
                array_map(fn(Redirection $redirection): string => $redirection->url, $this->redirections),
                array_map(fn(Redirection $redirection): ?int => $redirection->numberOfRedirections, $this->redirections)
            )
        );
    }

    public function toArray(): array
    {
        return array_map(fn(Redirection $redirection) => [
            'numberOfRedirections' => $redirection->numberOfRedirections,
            'time' => number_format($redirection->time, 5, '.', ''),
            'checkedAt' => $redirection->checkedAt ? $redirection->checkedAt->format('Y-m-d H:i:s') : null,
        ], $this->redirections);
    }
}

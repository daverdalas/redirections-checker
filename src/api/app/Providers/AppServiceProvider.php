<?php

namespace App\Providers;

use App\Repositories\Eloquent\RedirectionRepository;
use App\Repositories\RedirectionRepositoryInterface;
use App\Services\RedirectsChecker\RedirectsCheckerServiceProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public array $bindings = [
        RedirectionRepositoryInterface::class => RedirectionRepository::class,
    ];

    public function register(): void
    {
        $this->app->register(RedirectsCheckerServiceProvider::class);
    }
}

<?php

namespace App\Jobs;

use App\Repositories\RedirectionRepositoryInterface;
use App\Services\RedirectsChecker\RedirectsChecker;
use App\Services\RedirectsChecker\UrlStats;
use App\Entities\Redirection;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

final class CheckUrlsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var string[] */
    private array $urls;

    /**
     * @param string[] $urls
     */
    public function __construct(array $urls)
    {
        $this->urls = $urls;
    }

    public function handle(
        RedirectsChecker $redirectsChecker,
        RedirectionRepositoryInterface $redirectionRepository
    ): void {
        $results = $redirectsChecker($this->urls);
        $redirectionRepository->saveMany(array_map(
            fn(UrlStats $urlStats): Redirection => Redirection::fromUrlStats($urlStats, new \DateTimeImmutable()),
            $results
        ));
    }
}

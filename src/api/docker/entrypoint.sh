#!/bin/bash

set -x -e

/usr/bin/supervisord -c /etc/supervisord.conf &

/usr/sbin/crond &

su - www-data

exec "$@"

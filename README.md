# Redirections checker

## Task description

https://gitlab.com/daverdalas/redirections-checker/-/blob/master/Senior_PHP_Developer_-_Laravel_task_v2.pdf

## Installation

- Make sure you have docker and docker-compose installed in the latest versions.
- Type `make install` from command line
- Go to `http://url-checker/` and check that the page displays ok without any errors
- That's all :) Supervisor should be running, cron should push urls to check every minute.
- The code has been tested on Mac OS and Linux but there is always a possibility that something 
  may not work. In this case feel free to contact me at: suchanekpiotr@gmail.com.

## Usage

- `make test` To run simple tests
- `make seed` To seed database with example urls to check

## API

- **Push ulrs to monitor:** POST urls as simple JSON array to `http://url-checker/monitors`.
  When `stats` query argument is present and set to `true` API
  will return the initial results it has managed to acquire during the 2s runtime in `X-Stats` header.
  Example:
  `curl --location --request POST 'url-checker/monitors?stats=true' \
  --header 'Content-Type: application/json' \
  --data-raw '[
  "https://www.onet.pl",
  "http://test-redirects.137.software"]'`
  
- **Url info:** Send GET request to  `http://url-checker/monitors/{url}`. 
  Remeber to enocde url for query:  https://www.urlencoder.org/. Example:
  `curl --location --request GET 'url-checker/monitors/http%3A%2F%2Ftest-redirects.137.software%3Frand%3D49' \
  --data-raw ''`
  
## Performance tweaks

- Using the stream option on Guzzle. We don't need a body response from the server. 
  Reduces the time it takes to build the response and memory usage.
- Database indexes
- Using batch insert to the database to reduce the number of connections, and the number of queries.
- Using guzzle pool to check several urls at the same time.
- Pushing several urls to one Query Job to maximize the benefits of the points described above.
- You can customize the above tweaks to your needs using the configuration file: `redirects_checker.php`
  
## About the requirements

- **Time limit 2s:** By setting a query timeout and using concurrency I was able to optimize the number of results that 
  we can get in 2s time. However, it takes some time to build response.  
  If we would like to get the answer exactly at 2s we should use more asynchronous language like JavaScript (Node)
- **DDD:** For the past 2 years I have worked for a company heavily focused on DDD. I know the advantages and risks of 
  using it. I've seen many examples in which developers fell into a rabbit hole of abstraction and multiplication of 
  objects. Such code was highly inefficient, hard to test, and creating new functionality was difficult and time-consuming. 
  So we tried a new approach, creating super-simple microservices and forgetting about DDD. Work was much 
  faster and more efficient. I'm not saying DDD is bad, just that you have to know its pros and cons.I like to use 
  different elements that are often used together with DDD like CQRS, ValueObjects, Event Sourcing and so on but don't 
  overuse abstractions. I'm always a fan of creating something simple, and only when the need appears introducing more 
  complex solutions. However, I would love to see full DDD, scaled up, with high test coverage. Because the real world 
  and real problems did not give me such example. I'm always a fan of creating something simple, and only when the need
  appears introducing more complex solutions. However, I would love to see full DDD, scaled up, with high test coverage. 
  Because the real world and real problems did not give me such an example. I would be happy to discuss this matter :)

UID=$(shell id -u)
ARGS=USER_UID=${UID}

install:
	if grep -q "url-checker" /etc/hosts; then \
		echo "Virtual host exists"; \
	else \
		echo '127.0.0.1	url-checker' | sudo tee -a /etc/hosts; \
	fi
	${ARGS} docker-compose up -d --force-recreate --build
	${ARGS} docker-compose exec --user=www-data api composer install
	${ARGS} docker-compose exec --user=www-data api php artisan migrate

test:
	${ARGS} docker-compose up -d
	${ARGS} docker-compose exec --user=www-data api phpunit

seed:
	${ARGS} docker-compose exec --user=www-data api php artisan migrate:fresh --seed